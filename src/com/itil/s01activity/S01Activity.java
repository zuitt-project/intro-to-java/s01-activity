package com.itil.s01activity;

public class S01Activity {

    public static void main(String[] args) {

        char initial = 'C';
        int age = 18;
        char gender = 'F';
        boolean isSingle = true;

        System.out.println("Initial: " + initial + "\nAge: " + age + "\nGender: " + gender + "\nCivil Status: " + isSingle);
    };
}
